# Zbrush-Batch
This tools setup all export to work with Substance painter.

![Help](ScreenPlugin.jpg)

# Dev
The folder **Plugins** contains all binaries generate with Zbrush, and **Sources** the plugin dev.

To quickly try a specific feature, write the code on *Debug.txt*.

# Install
Paste all files present in the **Plugins** folder on your Zplugs folder (present in your Zbrush install "C:\Program
 Files\Pixologic\ZBrush 2020\ZStartup\ZPlugs").

> The sub folder *BatExport* contains all dependance needed. Don't forget to paste it on your Zplugs folder.
